package org.eyo.architecture.hexagonal.ecommerce.shoppingcart.application.service;

import org.eyo.architecture.hexagonal.ecommerce.shoppingcart.domain.dto.ShoppingCartDTO;
import org.eyo.architecture.hexagonal.ecommerce.shoppingcart.domain.port.EcommerceManager;

public class EcommerceMockManager implements EcommerceManager {

    @Override
    public Boolean validateCart(ShoppingCartDTO shoppingCart) {
        if (shoppingCart.getCartItems().size() % 2 == 0) {
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }
}
