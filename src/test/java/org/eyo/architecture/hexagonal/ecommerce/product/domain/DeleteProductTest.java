package org.eyo.architecture.hexagonal.ecommerce.product.domain;

import org.eyo.architecture.hexagonal.ecommerce.product.domain.port.ProductUseCase;
import org.eyo.architecture.hexagonal.ecommerce.product.domain.dto.ProductDTO;
import org.eyo.architecture.hexagonal.ecommerce.product.domain.dto.CreateProductRequestDTO;
import org.eyo.architecture.hexagonal.ecommerce.product.infraestructure.repository.ProductMockRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.eyo.architecture.hexagonal.ecommerce.TestConstants.*;
import static org.junit.jupiter.api.Assertions.*;

class DeleteProductTest {

    private ProductUseCase productUseCase;

    @BeforeEach
    void setUp() {
        this.productUseCase = new ProductUseCaseImpl(new ProductMockRepository());
    }

    @Test
    void givenCreatedProduct_whenDeleteProduct_shouldNotBeTheProductAvailable() {
        Long productIdCreated = this.createProductExample().getId();
        assertNotNull(this.productUseCase.getProduct(productIdCreated));

        ProductDTO deletedProduct = this.productUseCase.deleteProduct(productIdCreated);

        assertEquals(productIdCreated, deletedProduct.getId());
        assertNull(this.productUseCase.getProduct(productIdCreated));
    }

    private ProductDTO createProductExample() {
        return this.productUseCase.create(new CreateProductRequestDTO(BOOK_KIND, BOOK_NAME, BOOK_DESCRIPTION));
    }
}
