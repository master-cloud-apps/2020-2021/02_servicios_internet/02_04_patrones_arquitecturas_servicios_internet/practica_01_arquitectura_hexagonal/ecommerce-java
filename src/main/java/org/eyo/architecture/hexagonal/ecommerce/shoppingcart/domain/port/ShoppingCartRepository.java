package org.eyo.architecture.hexagonal.ecommerce.shoppingcart.domain.port;


import org.eyo.architecture.hexagonal.ecommerce.shoppingcart.domain.dto.ShoppingCartDTO;

public interface ShoppingCartRepository {
    ShoppingCartDTO save(ShoppingCartDTO shoppingCartDTO);

    ShoppingCartDTO findById(Long shoppingCartId);

    ShoppingCartDTO deleteById(Long shoppingCartId);

}
