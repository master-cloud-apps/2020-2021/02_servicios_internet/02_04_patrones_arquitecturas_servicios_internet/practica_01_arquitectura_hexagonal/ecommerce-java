package org.eyo.architecture.hexagonal.ecommerce.product.application.service;

import org.eyo.architecture.hexagonal.ecommerce.product.domain.port.ProductUseCase;
import org.eyo.architecture.hexagonal.ecommerce.product.domain.dto.CreateProductRequestDTO;
import org.eyo.architecture.hexagonal.ecommerce.product.domain.dto.ProductDTO;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductServiceImpl implements ProductService {
    private ProductUseCase productUseCase;

    public ProductServiceImpl(ProductUseCase productUseCase) {
        this.productUseCase = productUseCase;
    }

    @Override
    public ProductDTO create(CreateProductRequestDTO createProductRequestDTO) {
        return this.productUseCase.create(createProductRequestDTO);
    }

    @Override
    public ProductDTO getProduct(Long productId) {
        return this.productUseCase.getProduct(productId);
    }

    @Override
    public ProductDTO updateProduct(ProductDTO productToUpdate) {
        return this.productUseCase.updateProduct(productToUpdate);
    }

    @Override
    public ProductDTO deleteProduct(Long productId) {
        return this.productUseCase.deleteProduct(productId);
    }

    @Override
    public List<ProductDTO> getProducts() {
        return this.productUseCase.getProducts();
    }
}
