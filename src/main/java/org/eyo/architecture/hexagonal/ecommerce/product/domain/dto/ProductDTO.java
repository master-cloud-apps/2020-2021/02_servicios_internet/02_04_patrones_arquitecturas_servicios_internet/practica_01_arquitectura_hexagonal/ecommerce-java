package org.eyo.architecture.hexagonal.ecommerce.product.domain.dto;

import org.eyo.architecture.hexagonal.ecommerce.product.domain.Product;

public class ProductDTO {
    private Long id;
    private String name;
    private String kind;
    private String description;

    public ProductDTO() {
    }

    public ProductDTO(Product product) {
        this.id = product.getId();
        this.name = product.getName();
        this.kind = product.getKind();
        this.description = product.getDescription();
    }

    public ProductDTO(CreateProductRequestDTO productToCreate) {
        this.name = productToCreate.getName();
        this.kind = productToCreate.getKind();
        this.description = productToCreate.getDescription();
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
