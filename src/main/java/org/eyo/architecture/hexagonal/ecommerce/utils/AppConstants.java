package org.eyo.architecture.hexagonal.ecommerce.utils;

public class AppConstants {

    private AppConstants() {
    }

    public static final String PRODUCT_URL = "/api/products";
    public static final String SHOPPING_CART_URL = "/api/shoppingcarts";
}
