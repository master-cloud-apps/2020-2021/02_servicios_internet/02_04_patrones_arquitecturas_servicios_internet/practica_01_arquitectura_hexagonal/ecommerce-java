package org.eyo.architecture.hexagonal.ecommerce.shoppingcart.domain;

import org.eyo.architecture.hexagonal.ecommerce.product.domain.Product;
import org.eyo.architecture.hexagonal.ecommerce.shoppingcart.domain.dto.ShoppingCartDTO;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class ShoppingCart {
    private Long id;
    private String status;
    private List<CartItem> cartItems;

    public ShoppingCart(ShoppingCartDTO shoppingCartDTO) {
        this.id = shoppingCartDTO.getId();
        this.status = shoppingCartDTO.getStatus();
        this.cartItems = this.mapItemsFromDTO(shoppingCartDTO);
    }

    private List<CartItem> mapItemsFromDTO(ShoppingCartDTO shoppingCartDTO){
        if (shoppingCartDTO.getCartItems() != null){
            return shoppingCartDTO.getCartItems().stream().map(CartItem::new).collect(Collectors.toList());
        }
        return new ArrayList<>();
    }

    public ShoppingCart includeProductInQuantity(Product productToInclude, Integer quantity) {
        if (this.findCartItemByProductId(productToInclude).isPresent()) {
            this.findCartItemByProductId(productToInclude).get().setQuantity(quantity);
            return this;
        }
        this.cartItems.add(new CartItem(productToInclude, quantity));
        return this;
    }

    private Optional<CartItem> findCartItemByProductId(Product product) {
        return this.cartItems.stream().filter(cartItem -> cartItem.getProductId().equals(product.getId())).findFirst();
    }

    public Long getId() {
        return id;
    }

    public String getStatus() {
        return status;
    }

    public List<CartItem> getCartItems() {
        return cartItems;
    }

    public ShoppingCart deleteProduct(Long productId) {
        this.cartItems =
                this.cartItems.stream().filter(item -> !item.getProductId().equals(productId)).collect(Collectors.toList());
        return this;
    }
}
