package org.eyo.architecture.hexagonal.ecommerce;

import org.eyo.architecture.hexagonal.ecommerce.product.domain.ProductUseCaseImpl;
import org.eyo.architecture.hexagonal.ecommerce.product.domain.port.ProductRepository;
import org.eyo.architecture.hexagonal.ecommerce.product.domain.port.ProductUseCase;
import org.eyo.architecture.hexagonal.ecommerce.shoppingcart.domain.ShoppingCartUseCaseImpl;
import org.eyo.architecture.hexagonal.ecommerce.shoppingcart.domain.port.EcommerceManager;
import org.eyo.architecture.hexagonal.ecommerce.shoppingcart.domain.port.ShoppingCartRepository;
import org.eyo.architecture.hexagonal.ecommerce.shoppingcart.domain.port.ShoppingCartUseCase;
import org.modelmapper.ModelMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class EcommerceApplication {

    public static void main(String[] args) {
        SpringApplication.run(EcommerceApplication.class, args);
    }

    @Bean
    public ModelMapper getModelMapper() {
        return new ModelMapper();
    }

    @Bean
    public ProductUseCase getProductUseCase(ProductRepository productRepository) {
        return new ProductUseCaseImpl(productRepository);
    }

    @Bean
    public ShoppingCartUseCase getShoppingCartUseCase(ShoppingCartRepository shoppingCartRepository,
                                                      ProductRepository productRepository
            , EcommerceManager ecommerceManager) {
        return new ShoppingCartUseCaseImpl(shoppingCartRepository, productRepository, ecommerceManager);
    }

}
