package org.eyo.architecture.hexagonal.ecommerce.product.application.controller;

import org.eyo.architecture.hexagonal.ecommerce.product.application.service.ProductService;
import org.eyo.architecture.hexagonal.ecommerce.product.domain.dto.CreateProductRequestDTO;
import org.eyo.architecture.hexagonal.ecommerce.product.domain.dto.ProductDTO;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.Collection;

import static org.eyo.architecture.hexagonal.ecommerce.utils.AppConstants.PRODUCT_URL;
import static org.springframework.web.servlet.support.ServletUriComponentsBuilder.fromCurrentRequest;

@RestController
@RequestMapping(PRODUCT_URL)
public class ProductController {

    private final ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping("")
    public ResponseEntity<Collection<ProductDTO>> getProducts() {
        return ResponseEntity.ok(this.productService.getProducts());
    }

    @PostMapping("")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<Void> createProduct(@RequestBody CreateProductRequestDTO productRequest){
        ProductDTO createdProduct = productService.create(productRequest);
        URI location = fromCurrentRequest().path("/{id}").buildAndExpand(createdProduct.getId()).toUri();

        return ResponseEntity.created(location).build();
    }

    @GetMapping("/{productId}")
    public ResponseEntity<ProductDTO> getProductById(@PathVariable Long productId) {
        if (this.productService.getProduct(productId) == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(this.productService.getProduct(productId));
    }

    @DeleteMapping("/{productId}")
    public ResponseEntity<Void> deleteProductById(@PathVariable Long productId) {
        ProductDTO productToDelete = this.productService.getProduct(productId);

        if (productToDelete == null) {
            return ResponseEntity.notFound().build();
        }
        this.productService.deleteProduct(productId);
        return ResponseEntity.noContent().build();
    }


}
