package org.eyo.architecture.hexagonal.ecommerce.product.application.service;

import org.eyo.architecture.hexagonal.ecommerce.product.domain.dto.CreateProductRequestDTO;
import org.eyo.architecture.hexagonal.ecommerce.product.domain.dto.ProductDTO;

import java.util.List;

public interface ProductService {
    ProductDTO create(CreateProductRequestDTO createProductRequestDTO);

    ProductDTO getProduct(Long productId);

    ProductDTO updateProduct(ProductDTO productToUpdate);

    ProductDTO deleteProduct(Long productId);

    List<ProductDTO> getProducts();
}
